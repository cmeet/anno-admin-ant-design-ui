import SvgIcon from './src/SvgIcon.vue';
import Icon from './Icon.vue';
import IconPicker from './src/IconPicker.vue';

export { IconPicker, SvgIcon, Icon };
