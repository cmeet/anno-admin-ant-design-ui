import { defHttp } from '@/utils/http/axios';
import {LoginParams, LoginResultModel, GetUserInfoModel, ImageCodeModel, UpdatePwdParams} from './model/userModel';

import { ErrorMessageMode } from '#/axios';

enum Api {
  Login = '/anno-admin-api/system/auth/login',
  Logout = '/anno-admin-api/system/auth/logout',
  GetUserInfo = '/anno-admin-api/system/auth/me',
  GetPermCode = '/basic-api/getPermCode',
  TestRetry = '/basic-api/testRetry',
  ImageCode = '/anno-admin-api/system/common/captcha',
  ClearCache = '/anno-admin-api/system/auth/clearSysUserCache',
  UpdatePwd = '/anno-admin-api/system/auth/updatePwd',
}

/**
 * @description: user login api
 */
export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<LoginResultModel>(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: clearCache
 */
export function clearCache() {
  return defHttp.post<any>({ url: Api.ClearCache }, { errorMessageMode: 'none' });
}
export function getUserInfo() {
  return defHttp.post<GetUserInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}
export function getPermCode() {
  return defHttp.get<string[]>({ url: Api.GetPermCode });
}

export function getImageCode() {
  return defHttp.get<ImageCodeModel>({ url: Api.ImageCode });
}

export function doLogout() {
  return defHttp.post({ url: Api.Logout });
}

/**
 * @description: user updatePwd api
 */
export function updatePwdApi(params: UpdatePwdParams) {
    return defHttp.post<LoginResultModel>(
        {
            url: Api.UpdatePwd,
            params,
        }
    );
}

export function testRetry() {
  return defHttp.get(
    { url: Api.TestRetry },
    {
      retryRequest: {
        isOpenRetry: true,
        count: 5,
        waitTime: 1000,
      },
    },
  );
}
