import { AnEntity, DictModel } from './model/entitysModel';
import {DictParams, TransferData} from './model/AnnoModel';
import { defHttp } from '@/utils/http/axios';
import { UploadApiResult } from './model/uploadModel';
import { UploadFileParams } from '#/axios';
import { AxiosProgressEvent } from 'axios';
import {GlobalConfig} from "@/api/sys/model/GlobalConfig";

export enum Api {
  getEntity = '/anno-admin-api/system/config/anEntity/',
  getDict = '/anno-admin-api/amis/system/dict/loadDict',
  transOne = '/anno-admin-api/amis/system/dict/transOne',
  getTableData = '/anno-admin-api/amis/system/anno',
  upload = '/anno-admin-api/api/upload',
  save = '/anno-admin-api/amis/system/anno/{clazz}/save',
  update = '/anno-admin-api/amis/system/anno/{clazz}/updateById',
  getOneChartData = '/anno-admin-api/amis/system/anno/{clazz}/oneChartData',
  delete = '/anno-admin-api/amis/system/anno/{clazz}/removeById',
  deleteM2m = '/anno-admin-api/amis/system/anno/{clazz}/remove-relation',
  addM2m = '/anno-admin-api/amis/system/anno/{clazz}/addM2m',
  treeStruct = '/anno-admin-api/amis/system/anno/{clazz}/annoTrees',
  annoTreeSelectData = '/anno-admin-api/amis/system/anno/{clazz}/annoTreeSelectData',
  javaCmd = '/anno-admin-api/amis/system/anno/{clazz}/runJavaCmd',
  globalConfig = '/anno-admin-api/api/global/config',
}

export function getEntity(clazz: string) {
  if (clazz == 'DefaultBaseForm') {
    // @ts-ignore
    return new Promise<AnEntity>((resolve) => {resolve( null)});
  }
  return defHttp.get<AnEntity>({ url: `${Api.getEntity}${clazz}` });
}

export function getDict(data: DictParams) {
  return defHttp.post<DictModel>({ url: Api.getDict, data });
}

export function transOne(data: TransferData) {
  return defHttp.post<String>({ url: Api.transOne, data });
}

export function getTableData(clazz: string, pidkey: string, idkey: string, isDisplyTree: boolean) {
  return (data: any) =>
    defHttp.post<any>({ url: Api.getTableData + `/${clazz}/` + 'page', data }).then((res) => {
      for (let index = 0; index < res.list.length; index++) {
        const element = res.list[index];
        for (const key in element) {
          element[key] = toJSON(element[key]);
        }
      }
      return res;
    });
}
function toJSON(param: any) {
  if (!param && param != 0) {
    return param;
  }
  if (param instanceof Object || param instanceof Array) {
    return param;
  }
  try {
    const paramObj = JSON.parse(param);
    if (paramObj instanceof Object || paramObj instanceof Array) {
      return paramObj;
    }
    return param.toString();
  } catch (err) {
    return param.toString();
  }
}

export function getOneChartData(clazz: string, id:string, data: any) {
  data = {
    ...data,
    'annoChartFieldId': id
  }
  return defHttp.post<any>({ url: Api.getOneChartData.replace('{clazz}', clazz), data });
}

export function saveData(clazz: string, data: any) {
  return defHttp.post<any>({ url: Api.save.replace('{clazz}', clazz), data });
}

export function updateData(clazz: string, data: any) {
  return defHttp.post<any>({ url: Api.update.replace('{clazz}', clazz), data });
}

export function deleteData(clazz: string, data: any) {
  return defHttp.post<any>({ url: Api.delete.replace('{clazz}', clazz), data });
}

export function deleteM2m(clazz: string, data: any) {
  return defHttp.post<any>({ url: Api.deleteM2m.replace('{clazz}', clazz), data });
}

export function addM2ms(clazz: string, data: any) {
  return defHttp.post<any>({ url: Api.addM2m.replace('{clazz}', clazz), data });
}

export function treeStruct(clazz: string, data: any) {
  return defHttp.post<any>({ url: Api.treeStruct.replace('{clazz}', clazz), data });
}

export function annoTreeData(clazz: string, data: any) {
  return defHttp.post<any>({ url: Api.annoTreeSelectData.replace('{clazz}', clazz), data });
}

export function runJavaCmd(clazz: string, data: any) {
  return defHttp.post<any>({ url: Api.javaCmd.replace('{clazz}', clazz), data });
}

export function uploadApi(
  params: UploadFileParams,
  onUploadProgress: (progressEvent: AxiosProgressEvent) => void,
) {
  return defHttp.uploadFile<UploadApiResult>(
    {
      url: Api.upload,
      onUploadProgress,
    },
    params,
  );
}

export function globalConfig() {
    return defHttp.get<GlobalConfig>({ url: Api.globalConfig});
}
