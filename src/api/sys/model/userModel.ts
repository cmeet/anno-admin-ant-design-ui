/**
 * @description: Login interface parameters
 */
export interface LoginParams {
  username: string;
  password: string;
  code: string;
  codeKey: string;
}

export interface RoleInfo {
  roleName: string;
  value: string;
}

/**
 * @description: Login interface return value
 */
export type LoginResultModel = string;

/**
 * @description: Get user information return value
 */
export interface GetUserInfoModel {
  roles: string[];
  // 用户名
  name: string;
  // 头像
  avatar: string;
}

export interface ImageCodeModel {
  key: string;
  image: string;
}

export interface UpdatePwdParams {
  oldPwd: string;
  newPwd1: string;
  newPwd2: string;
}
