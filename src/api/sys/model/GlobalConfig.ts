export interface GlobalConfig {
    name?: string;
    platformLogo?: string;
    description?: string;
}
