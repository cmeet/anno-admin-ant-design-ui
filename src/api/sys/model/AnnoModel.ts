export interface DictParams {
  sqlKey?: string;
  annoClazz?: string;
  idKey?: string;
  labelKey?: string;
  optionAnnoClazz?: string;
  treeAnnoClazz?: string;
  _extra?: string;
}

export interface TransferData {
  annoClazz?: string;
  idKey?: string;
  labelKey?: string;
  idValue?: string;
}
export interface AnnoTree {
  id: string;
  key: string;
  label: string;
  title: string;
  value: string;
  children: AnnoTree[];
}
