export interface DictModel {
  id: string;
  label: string;
  title: string;
  value: string;
  key: string;
  parentId?: string;
  children: DictModel[];
}


export interface Search {
  notNull: boolean;
  enable: boolean;
  queryType: string;
  defaultValue: string;
  placeHolder: string;
}

export interface ShowBy {
  enable: boolean;
  expr: string;
}

export interface Edit {
  notNull: boolean;
  placeHolder: string;
  span: number;
  addEnable: boolean;
  editEnable: boolean;
  canClear: boolean;
  showBy: ShowBy;
}

export interface OptionAnno {
  annoClass: string;
  idKey: string;
  labelKey: string;
}

export interface CodeType {
  mode: string;
  theme: string;
};

export interface ImageType {
  enlargeAble: boolean;
  width: number;
  height: number;
};

export interface TreeAnno {
  annoClass: string;
  idKey: string;
  labelKey: string;
  pidKey: string;
};

export interface TreeTypeValue{
  id: string;
  label: string;
  pid: string;
}

export interface TreeType {
  sqlKey: string;
  sql: string;
  value: Array<TreeTypeValue>;
  supplier: string;
  treeAnno: TreeAnno;
  isMultiple: boolean;
};

export interface FileType {
  fileType: string;
  fileMaxCount: number;
  fileMaxSize: number;
};

export interface AnField {
  javaName: string;
  javaType: string;
  title: string;
  tableFieldName: string;
  fieldSize: number;
  show: boolean;
  search: Search;
  edit: Edit;
  dataType: string;
  optionType: OptionType;
  codeType: CodeType;
  imageType: ImageType;
  treeType: TreeType;
  fileType: FileType;
  virtualColumn: boolean;
  pkField: boolean;
  sort: number;
  updateWhenNullSet: string;
  insertWhenNullSet: string;
};

export interface OptionValue {
  label: string;
  value: string;
};

export interface OptionType {
  sqlKey: string;
  sql: string;
  value: Array<OptionValue>;
  supplier: string;
  optionEnum: string;
  optionAnno: OptionAnno;
  isMultiple: boolean;
};


export interface AnnoLeftTree {
  leftTreeName: string;
  catKey: string;
  treeClass: string;
  enable: boolean;
};

export interface AnnoPermission {
  enable: boolean;
  baseCode: string;
  baseCodeTranslate: string;
};

export interface AnnoRemove {
  removeType: number;
  removeValue: string;
  notRemoveValue: string;
  removeField: string;
};

export interface AnnoTree {
  label: string;
  parentKey: string;
  key: string;
  displayAsTree: boolean;
  enable: boolean;
};

export interface AnnoChart {
  enable: boolean;
  layout: Array<number>;
  searchForm: string;
  chartFields: Array<ChartFiled>;
}

export interface ChartFiled {
  id: string
  name: string
  type: string
  runSupplier: string
  order: number
  permissionCode: string
  actionColor: string
  action: string
}

export interface O2mJoinButton {
  targetClass: string
  thisJavaField: string
  targetJavaField: string
  enable: boolean
}

export interface M2mJoinButton {
  id: string
  joinTargetClazz: string
  joinThisClazzField: string
  joinTargetClazzField: string
  mediumTableClazz: string
  mediumTargetField: string
  mediumThisField: string
  windowSize: string
  enable: boolean
}

export interface JavaCmd {
  id: string
  runSupplier: string
  enable: boolean
}

export interface AnnoTpl {
  id: string
  tplClazz: string
  windowWidth: string
  windowHeight: string
  enable: boolean
}


export interface ColumnButton {
  name: string
  permissionCode: string
  icon: string
  size: string
  baseForm: string
  jsCmd: string
  jumpUrl: string
  o2mJoinButton: O2mJoinButton
  m2mJoinButton: M2mJoinButton
  javaCmd: JavaCmd
  annoTpl: AnnoTpl
}

export interface AnEntity {
  autoMaintainTable: boolean;
  annoLeftTree: AnnoLeftTree;
  pkColumn: AnField;
  columns: Array<AnField>;
  tableButtons: Array<ColumnButton>;
  virtualTable: boolean;
  tableName: string;
  thisClass: string;
  extend: string;
  annoPermission: AnnoPermission;
  annoRemove: AnnoRemove;
  canRemove: boolean;
  annoTree: AnnoTree;
  columnButtons: Array<ColumnButton>;
  annoOrder: Array<unknown>;
  entityName: string;
  annoChart: AnnoChart;
  name: string;
  annoTableButton: Array<unknown>;
  orgFilter: boolean;
}
