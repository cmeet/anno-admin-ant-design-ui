import type { GlobConfig } from '#/config';

import { getAppEnvConfig } from '@/utils/env';
import {GlobalConfig} from "@/api/sys/model/GlobalConfig";

export const useGlobSetting = (): Readonly<GlobConfig> => {
  const { VITE_GLOB_APP_TITLE, VITE_GLOB_API_URL, VITE_GLOB_API_URL_PREFIX, VITE_GLOB_UPLOAD_URL } =
    getAppEnvConfig();
  const storage:GlobalConfig = JSON.parse(localStorage.getItem('globalConfig') || '{}');
  // Take global configuration
  const glob: Readonly<GlobConfig> = {
    title: storage.name || VITE_GLOB_APP_TITLE,
    apiUrl: VITE_GLOB_API_URL,
    shortName: VITE_GLOB_APP_TITLE.replace(/\s/g, '_').replace(/-/g, '_'),
    urlPrefix: VITE_GLOB_API_URL_PREFIX,
    uploadUrl: VITE_GLOB_UPLOAD_URL,
  };
  return glob as Readonly<GlobConfig>;
};
