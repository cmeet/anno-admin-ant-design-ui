import { ChartFiled } from "@/api/sys/model/entitysModel";
import { Component, DefineComponent, VNode } from "vue";
import { getOneChartData } from "@/api/sys/anno";
import NumberChart from "@/views/anno/chart/NumberChart.vue";
import CommonChart from "@/views/anno/chart/CommonChart.vue";
import PieChart from "@/views/anno/chart/PieChart.vue";
import LineChart from "@/views/anno/chart/LineChart.vue";
import BarChart from "@/views/anno/chart/BarChart.vue";

export interface IChartRenderField {
  title: string,
  valueFunc: Function,
  type: string,
  render: VNode | Component | DefineComponent
}

export function chartFieldRender(field: ChartFiled, entityClass: string, extraParam: {}): IChartRenderField {
  let render = {
    title: field.name,
    valueFunc: () => valueFetch(field, entityClass, extraParam),
    type: field.type,
    render: {} as VNode
  } as IChartRenderField;
  if (field.type === "NUMBER") {
    return {
      ...render,
      render: <NumberChart valueFetch={render.valueFunc} anChartField={field} />
    };
  } else if (field.type == "PIE") {
    return {
      ...render,
      render: <PieChart valueFetch={render.valueFunc} anChartField={field} />
    };
  } else if (field.type == "BAR") {
    return {
      ...render,
      render: <BarChart valueFetch={render.valueFunc} anChartField={field} />
    };
  } else if (field.type === "LINE") {
    return {
      ...render,
      render: <LineChart valueFetch={render.valueFunc} anChartField={field} />
    };
  } else {
    return {
      ...render,
      render: <CommonChart valueFetch={render.valueFunc} anChartField={field} />
    };
  }
}

const valueFetch = (field: ChartFiled, entityClass: string, extraParam: {}) => {
  return getOneChartData(entityClass, field.id, extraParam);
};
