import { BasicColumn } from '@/components/Table';
import { AnField } from "@/api/sys/model/entitysModel";
import { Image } from 'ant-design-vue';
import { Icon } from '@/components/Icon';
import IFrameModelButton from "@/components/IFrameModal/IFrameModelButton/index.vue"
import BasicPreview from "@/components/Upload/src/BasicPreview.vue";
import PickColors from "vue-pick-colors";
import {QrCode} from "@/components/Qrcode";
const ImageType: string[] = ['IMAGE','AVATAR'];
const IconType: string[] = ['ICON'];
const FilesType: string[] = ['FILE'];
const TransType: string[] = ['PICKER', 'OPTIONS', 'TREE', 'RADIO','CLASS_OPTIONS'];

export function getColumns(fieldsDatas: AnField[]){
  if (fieldsDatas && fieldsDatas.length > 0) {
    return fieldsDatas.map((item) => {
      let field: BasicColumn = {
        title: item.title,
        width: 120,
        annoType: item.dataType
      } as BasicColumn;
      field.dataIndex = item.javaName;
      field.defaultHidden = !item.show;
      field.sorter = { multiple: 1 };
      if (TransType.includes(item.dataType)) {
        field.customRender = ({ value, record }) => {
          console.log(value);
          console.log(record['joinResMap'][`${item.javaName.toLowerCase()}_label`]);
          return record['joinResMap'][`${item.javaName.toLowerCase()}_label`];
        };
      }
      if (ImageType.includes(item.dataType)) {
        field.customRender = ({ value, record }) => {
          let fallBack = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg==";
          return (
            <Image
              style={{ width: item.imageType.width + 'px', height: item.imageType.height + 'px' }}
              preview={value ? item.imageType.enlargeAble : false}
              src={value ? value : fallBack}
              fallback={fallBack}
            ></Image>
          );
        };
      }
      if ('COLOR' == item.dataType){
        field.customRender = ({ value, record }) => {
          return (
              <PickColors
                  show-picker={false}
                  showAlpha={true}
                  width={80}
                  height={20}
                  value={value}
              ></PickColors>
          );
        };
      }
      if ('RICH_TEXT' == item.dataType){
        field.customRender = ({ value, record }) => {
          return (
              <IFrameModelButton doc={value}></IFrameModelButton>
          );
        };
      }
      if ('MARK_DOWN' == item.dataType){
        field.customRender = ({ value, record }) => {
          return (
              <IFrameModelButton markdownDoc={value}/>
          );
        };
      }
      if ('CODE_EDITOR' == item.dataType){
        field.customRender = ({ value, record }) => {
          return (
            <IFrameModelButton codeDoc={value} codeMode={item.codeType.mode} codeTheme={item.codeType.theme} />
          );
        };
      }
      if ('LINK' == item.dataType){
        field.customRender = ({ value, record }) => {
          return (
              <IFrameModelButton link={value}></IFrameModelButton>
          );
        };
      }
      if ('QR_CODE' == item.dataType){
        field.customRender = ({ value, record }) => {
          return (
              <QrCode value={value}  previewWidth="60px" width="600" ></QrCode>
          );
        };
      }
      if (IconType.includes(item.dataType)) {
        field.customRender = ({ value, record }) => {
          return (
              <Icon
                  icon={value}
              ></Icon>
          );
        };
      }
      if (FilesType.includes(item.dataType)) {
        field.customRender = ({ value, record }) => {
          if (value instanceof Array) {
            return <BasicPreview value={value}> </BasicPreview>;
          }
          if (value){
            return <BasicPreview value={[value]}></BasicPreview>;
          }
          return <BasicPreview></BasicPreview>
        };
      }
      return field;
    });
  }
  return [];
}
