<div align="center">

[![license](https://img.shields.io/github/license/anncwb/vue-vben-admin.svg)](LICENSE)

<h1>Anno Admin Ant Design UI</h1>
<h3>Fork From <a href="https://doc.vvbin.cn/">Vue vben admin</a></h3>
<h4>感谢优秀的Vben Admin 和AntDesign Vue等这样优秀的开源项目</h4>
</div>

## 简介

Anno Admin Ant Design UI 与 AnnoAdmin 相配套使用的前端 UI 框架.

## 构建
```shell
npm i -g pnpm

# 安装依赖
pnpm install

# 本地开发
pnpm dev
```
