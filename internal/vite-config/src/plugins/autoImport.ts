/*
 * @Description:
 * @Version: 1.0
 * @Author: wale1994
 * @Date: 2023-05-12 09:45:30
 */
import AutoImport from 'unplugin-auto-import/vite';

export function configAutoImport() {
  return AutoImport({
    dts: 'types/auto-imports.d.ts',
    imports: [
      'vue',
      'pinia',
      'vue/macros',
      'vue-router',
      {
        '@vueuse/core': [],
      },
    ],
  });
}
